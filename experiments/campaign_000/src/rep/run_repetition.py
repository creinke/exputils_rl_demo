#!/usr/bin/env python
import rl_lib
from repetition_config import config

# run experiment
rl_lib.exp.run_rl_training(config=config)

