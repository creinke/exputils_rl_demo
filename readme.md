
Current version: 0.0.2 (09/12/2022)

Based on exputils: 0.3.1.

# Introduction

A template for reinforcement learning experiments using the [exputils](https://gitlab.inria.fr/creinke/exputils) library.

Experiment Utilities (exputils) contains various tools for the management of scientific experiments and their experimental data.
It is especially designed to handle experimental repetitions, including to run different repetitions, to effectively store and load data for them, and to visualize their results.  

Main features:
* Easy definition of default configurations using nested python dictionaries.
* Setup of experimental configuration parameters using an ODF file.
* Running of experiments and their repetitions locally or on clusters.
* Logging of experimental data (numpy, json).
* Loading and filtering of experimental data.
* Interactive Jupyter widgets to load, select and plot data as line, box and bar plots.  

# Setup

The exputils library can be used to run experiments on different locations:
 * on your local pc
 * INRIA workstations, such as hydra or ursa
 * INRIA GPU cluster
 * GRICAD

For each platform are seperate installation steps necessary.
For all platforms is the installation on the local pc necessary, as it works as the base station.
If you have no extra PC and set up your INRIA base station as your local, then follow the instructions of both Local and INRIA workstations.


## Local

### 1) Demo Project Code

It is recommended to have a `projects` folder on your machine under which you store your different projects.
For example: `/home/mario/dev/projects`

Clone via Git or download the current version of the demo project code (<https://gitlab.inria.fr/creinke/exputils_rl_demo>) and place it under this projects folder.


### 2) Conda Environment

It is recommended to use a [conda environment](<https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html>) to install all required packages.
The follwing steps describe how to install the project.

Open the terminal inside the demo directory that you installed in the first step.

Create and activate a new conda environment (you could choose a different name than *exputils_rl_demo*):
```
conda create -n exputils_rl_demo python=3.10
conda activate exputils_rl_demo
```

### 3) Exputils Library

#### a) Exputils Python Library

Install the exputils library via pip:
```
pip install experiment-utilities
```

For using Jupyter GUIs with Jupyter Notebook, run the following commands to activate the required extensions:

```
jupyter contrib nbextension install --user
jupyter nbextension enable --py --sys-prefix widgetsnbextension
jupyter nbextension enable --py --sys-prefix qgrid
jupyter nbextension enable codefolding/main
jupyter nbextension enable collapsible_headings/main
```


#### b) Exputils Commands

The commands package provides bash commands that allow to generate experiments and run them locally or on a remote machine or cluster.
Get them from here: https://gitlab.inria.fr/creinke/exputils_commands

It is recommended to install them under your user account, such as here:

```git clone https://gitlab.inria.fr/creinke/exputils_commands.git ~/.exputils_commands```

For each project, that has its own libraries and codes, we have to set up a project file, that informs the exputils where it can find the code and experiments for this project and to define some other default parameters, such as the default machines at Inria that should be used to run experiments.

Create the exputils project file:

```cp ~/.exputils_commands/projects/TEMPLATE ~/.exputils_commands/projects/exputils_rl_demo```

Then you need to modify the file *~/.exputils_commands/projects/exputils_rl_demo* to adapt it to your system.

TODO: describe the project file in more detail

Several commands exist which make it easier to run experiments.
Add the following lines to your ~/.bashrc to add the commands to the PATH and to load environment varibles of your default project:

    #################################################
    ## EXPUTILS COMMANDS
    # define path to the exputils folder
    PATH_TO_EXPUTILS="~/.exputils_commands"
    if [ -f "$PATH_TO_EXPUTILS/commands/eu_setup.sh" ] ; then
            . "$PATH_TO_EXPUTILS/commands/eu_setup.sh" "$PATH_TO_EXPUTILS"
    fi
    # set default project for exputils commands
    export EU_DEFAULT_PRJ=<project_name>
    # activate the default project
    source eu_activate
    
    # add a text in front of each terminal prompt that indicated the active project
    # comment out if not wanted
    export PS1="(eu: \$EU_ACTIVE_PRJ) $PS1"
    #################################################


Don't forget to resource your bashrc afterwards:

```source ~/.bashrc```

You can test afterwards if it works correctly by using the command to get the active project:

```eu_active_project```

It should return "*exputils_rl_demo*".


### 3) Custom package: rl_lib

Install the included package that will have the base code for the project (here the *rl_lib* package).
The package will be installed in the developer mode (*-e* option), so that changes to the code can be used immediately without reinstalling it:

```pip install -e ./rl_lib```


## INRIA  Workstation

TODO

### INRIA workspace

Set up your workspace on INRIA scratch to have a similar structure as the local worlspace having a *code* and *experiments* folder.
It is also recommended to have *singularity* directory for your singularity containers.
Example:
```
/scratch/<workstation>/<username>/code
/scratch/<workstation>/<username>/experiments
/scratch/<workstation>/<username>/singularity
```
where `<workstation>` is the name of the workstation, for example *hydra*, and `<username>` is your INRIA username.

### 1) Project

Set up the relevant parameters in the project file ./code/exputils/projects/exputils_rl_demo



### Singularity to set up on Inria workstation:

https://gitlab.inria.fr/creinke/rlexp/-/tree/master/singularity


---
# Usage

The template folder contains two directories:

 - rl_exputils_template: Package with main code for the experiments such as the agents, environments and code to run experiments
 - experiments: Folder for different experiments that are based on the rl_exputils_template package and that are configured and executed using the exputils package.

To run the experiments, enter one of the experimental campaigns in the experiments folder:

`cd ../experiments/campaign_000`

Experiments can be configured by adapting their source code in the *src* folder and the ODS file *experiment_configurations.ods*.
To generate the code of the experiments and their repetitions, execute the *generate_experiments.sh* script:

`eu_local_generate_experiments`

To run the experiments (it also generates the experiment files), execute the *run_local_experiments.sh* script:

`eu_local_run_experiments`

After the experiments are finished, they can be analyzed using the *overview.ipynb* Jupyter notebook under the *analyze* directory.
To run Jupyter notebook execute:

`jupyter notebook`

You can also use tensorboard to visualize the data.
Execute the following to run in

`tensorboard --logdir ./experiments/tensorboard_logs/`

# <a name="team-members"></a>Development Team

* [Chris Reinke](http:www.scirei.net): <chris.reinke@inria.fr>
