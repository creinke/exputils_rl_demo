import exputils as eu

class Random:
    """
    Agent with a random policy.

    Paramters:
        n_steps_keep_action: defines how long a certain action is kept (default = 1)
    """

    @staticmethod
    def default_config():
        def_config = eu.AttrDict()

        # defines how long a certain action is kept
        def_config.n_steps_keep_action = 1

        return def_config


    def __init__(self, env, config=None, **kwargs):
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        if self.config.n_steps_keep_action <= 0:
            raise ValueError('n_steps_keep_action parameter must be larger than 0!')

        self.action_space = env.action_space

        self.last_action = None
        self.same_action_counter = 0


    def new_episode(self, state, info):
        self.last_action = None
        self.same_action_counter = 0


    def step(self, state, info):

        if self.last_action is None or self.same_action_counter >= self.config.n_steps_keep_action:
            # sample a random action
            action = self.action_space.sample()

            self.last_action = action
            self.same_action_counter = 1

        else:
            # continue to use the last action
            action = self.last_action
            self.same_action_counter += 1

        return action


    def update(self, transition):
        state, action, reward, next_state, done, info = transition

        # random agent does not learn something
