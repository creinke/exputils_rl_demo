import numpy as np
import exputils as eu
import exputils.data.logging as log


def run_rl_training(config=None, **kwargs):

    # define the default configuration
    default_config = eu.AttrDict(
        agent = eu.AttrDict(cls=None),
        env = eu.AttrDict(cls=None),
        n_episodes = 100,
        n_max_steps_per_epsiode = np.inf,
        log_to_tensorboard = True,
    )
    # set the config based on the default config, given config, and the given function arguments
    config = eu.combine_dicts(kwargs, config, default_config)

    # set random seeds with seed defined in the config
    eu.misc.seed(config)

    # create the environment from the configuration
    env = eu.misc.create_object_from_config(config.env)

    # create the agent from the configuration, it also needs as constructor argument the environment
    agent = eu.misc.create_object_from_config(config.agent, env)

    step = -1
    for episode in range(config.n_episodes):

        # update the status in the status file
        eu.misc.update_status('episode {} of {}'.format(episode + 1, config.n_episodes))

        step += 1

        obs, info = env.reset()
        agent.new_episode(obs, info)

        done = False
        step_per_episode = 0

        # log step information
        log.add_value('step', step)
        log.add_value('step_per_episode', step_per_episode)
        log.add_value('episode_per_step', episode)
        log.add_value('reward_per_step', np.nan, log_to_tb=config.log_to_tensorboard, tb_global_step=step)

        reward_per_episode = 0

        while not done and step_per_episode < config.n_max_steps_per_epsiode - 1:

            action = agent.step(obs, info)

            prev_obs = obs
            obs, reward, done, info = env.step(action)

            transition = (prev_obs, action, obs, reward, done, info)
            agent.update(transition)

            reward_per_episode += reward

            step += 1
            step_per_episode += 1

            # log step information
            log.add_value('step', step)
            log.add_value('step_per_episode', step_per_episode)
            log.add_value('episode_per_step', episode)
            log.add_value('reward_per_step', reward, log_to_tb=config.log_to_tensorboard, tb_global_step=step)

        # log episode information
        log.add_value('episode', episode)
        log.add_value('reward_per_episode', reward_per_episode, log_to_tb=config.log_to_tensorboard, tb_global_step=episode)

    # save the log
    log.save()
