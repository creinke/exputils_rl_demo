import gym
import exputils as eu


class Corridor(gym.Env):
    """
    Discrete corridor environment where the agent starts in the middle of a corridor. It can go left or right.
    The agent can get a reward by reaching either the right or left side of the corridor.
    """

    metadata = {'render.modes': ['human']}

    @staticmethod
    def default_config():
        dc = eu.AttrDict(
            length = 3,
            left_reward = 0,
            right_reward = 1,
            movement_punishment = -0.1,
        )
        return dc


    def __init__(self, config=None, **kwargs):
        super().__init__()
        self.config = eu.combine_dicts(kwargs, config, self.default_config())

        self.observation_space = gym.spaces.Discrete(self.config.length)
        self.action_space = gym.spaces.Discrete(2)  # left (0) and right (1)

        self.agent_pos = None


    def reset(self):

        # start in the middle of the corridor
        self.agent_pos = int(self.config.length/2)

        state = self.agent_pos
        info = dict()

        return state, info


    def step(self, action):

        reward = self.config.movement_punishment
        done = False

        # go left
        if action == 0 and self.agent_pos > 0:
            self.agent_pos -= 1

        # go right
        elif action == 1 and self.agent_pos < self.config.length - 1:
            self.agent_pos += 1

        # unknown action
        else:
            raise ValueError('Unknown action {!r}!'.format(action))

        # did the agent reach one of the corridor ends
        if self.agent_pos == 0:
            done = True
            reward = self.config.left_reward

        elif self.agent_pos == self.config.length - 1:
            done = True
            reward = self.config.right_reward

        obs = self.agent_pos
        info = dict()

        return obs, reward, done, info


    def render(self, mode='human', close=False):
        # Render the environment to the screen. Prints the current position of the agent.

        if mode == 'human':
            print(self.agent_pos)
        else:
            raise ValueError('Unknown rendering mode {!r}!'.format(mode))
