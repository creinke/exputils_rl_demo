# Introduction

To run the code on a cluster, we are using singularity to run the experiments in a virtual machine.
This document describes how to create and configure the singularity container.

# Installation of Singularity

See https://robotlearn.gitlabpages.inria.fr/wiki/singularity/ for more information.


# Creation of the Singularity Image

To create the image, we first create a *requirement.txt* file that contains all required python libraries for the project.
Copy for this all requirements from your setup.cfg files into the *requirement.txt* file. 

Then, the singularity image file can be created using the definition file *exputils_rl_demo.def*:

`sudo singularity build exputils_rl_demo.sif exputils_rl_demo.def`